## rpm project template - spring 2021

### Requirements
- [docker](https://docs.docker.com/desktop/)

that is it.

### Installation Instructions
#### VSCode
1. clone this repo
2. open this repo on vscode
3. the build task should execute. If the build task doesn't execute you run task `build`

### Terminal
1. clone this repo
2. from within the repo's directory, run `docker build -t rpm-kbai .`

### Running project
Edit the Agent.py file within the app directory. 

- run vscode task `run`
or
- run `docker run -v ${workspaceFolder}/app:/etc/app -t rpm-kbai`
the output should show up under the app directory in the form of csv files

#### Submission
To submit, run the zip task or `zip -z ${workspaceFolder}/project.zip app/*`. This will create a `project.zip` file for submission. 
